﻿Flask - Simple Example
======================
Ett grundläggande Flask-projekt som även använder SQLAlchemy, WTForms, Bootstrap och Moment.js. Förenklar management med Migrate och Script.

Kom igång
---------
Initiera databasen med:
```
python manage.py db init
```

För att skapa den första migreringen och även tillämpa den i din databas kör du följande. Detta gör du sedan varje gång du ändrat din databasmodell.
```
python manage.py db migrate -m "Initial migration."
python manage.py db upgrade
```

För att starta utvecklingsservern kör du följande kommando:
```
python manage.py runserver
```
