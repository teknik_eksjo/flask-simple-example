# -*- coding: utf-8 -*-

from flask import render_template, redirect, flash, url_for
from sqlalchemy import func, desc
from . import app, db
from .models import Person
from .forms import PersonForm


@app.route('/', methods = ['GET', 'POST'])
def index():
    form = PersonForm()

    history = Person.query.order_by(desc(Person.time)).limit(10)
    frequency =  db.session.query(Person.name, func.count(Person.name).label('freq')).group_by(Person.name).order_by(desc('freq')).limit(10)
    
    if form.validate_on_submit():
        person = Person(name = form.name.data)
        db.session.add(person)
        db.session.commit() 

        flash('Tack så mycket, {}!'.format(person.name))
        return redirect(url_for('index'))

    return render_template('index.html', form = form, history = history, frequency = frequency)